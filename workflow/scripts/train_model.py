from joblib import dump
from pathlib import Path 
import pandas as pd 
from hydra import compose, initialize 
from hydra.utils import instantiate
from scipy import sparse

initialize(config_path=r"../../conf")
cfg = compose(config_name=r"config")

# Check if input and output paths exist
input_features_path = snakemake.input.features
input_target_path = snakemake.input.target
output_model_path = snakemake.output[0]

if not Path(input_features_path).exists() or not Path(input_target_path).exists():
    raise FileNotFoundError("Input files not found.")

model_path = Path(output_model_path)
if not model_path.parent.exists():
    model_path.parent.mkdir()

X = sparse.load_npz(input_features_path)
y = pd.read_csv(input_target_path).target
model = instantiate(cfg.train.model)

# Fit the model
model.fit(X, y)

# Check if the model was successfully trained
if hasattr(model, "predict"):
    print("Model training completed successfully.")
else:
    raise ValueError("Model training failed.")

dump(model, output_model_path)