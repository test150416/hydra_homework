from pathlib import Path
import csv

from sklearn.datasets import fetch_20newsgroups

newsgroups_data = fetch_20newsgroups(subset='all', remove=('headers', 'footers', 'quotes'))
dataset_path = Path(snakemake.output[0])

if not dataset_path.parent.exists():
    dataset_path.parent.mkdir()
    
with open(dataset_path, mode='w', newline='', encoding='utf-8') as file:
    writer = csv.writer(file)
    writer.writerow(['text', 'target'])
    for text, category in zip(newsgroups_data.data, newsgroups_data.target):
        writer.writerow([text, category])