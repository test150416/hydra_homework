from pathlib import Path

import pandas as pd 
from scipy import sparse
from hydra.core.global_hydra import GlobalHydra
from hydra import compose, initialize 
from hydra.utils import instantiate

GlobalHydra.instance().clear()
initialize(config_path=r"../../conf")
cfg = compose(config_name=r"config")

dataset_path = Path(snakemake.input[0])
output_path = Path(snakemake.output[0])

df = (pd.read_csv(dataset_path)
      .sample(1000)
      .reset_index(drop=True)
      .dropna())

encoder = instantiate(cfg.preprocessing.encoder)
X_sparse_matrix = encoder.fit_transform(df.text)
y = df.target

sparse.save_npz(output_path, X_sparse_matrix)
y.to_csv(output_path.parent / 'target.csv')